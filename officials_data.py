import wget
import os
import time
import datetime


destino = os.getcwd() + '\\DataSet\\'
urlbase = 'https://datos.sfp.gov.py/data/'
archivo = 'periodos.csv'
csv_format = '.csv'
json_format = '.json'
ext_final = '.zip'
now = datetime.datetime.now()
anho_inicio = 2015


def generar_periodo():
    file = open(destino + archivo, 'w')
    for anho in range(anho_inicio, now.year + 1, 1):
        for mes in range(1, 12 + 1, 1):
            if anho == now.year and mes > now.month - 1:
                break
            file.writelines(f"funcionarios_{anho}_{mes}\n")
    file.close()


def descargar_archivo(opcion):
    file = open(destino + archivo, 'r')
    for line in file.readlines():
        uri = line.rstrip()
        if opcion == 1:
            uri += csv_format
        elif opcion == 2:
            uri += json_format
        uri += ext_final

        if not os.path.isfile(destino + uri):
            print(f"Descargando archivo:{uri}")
            wget.download(urlbase + uri, destino)
            time.sleep(10)
    file.close()


def opcion_menu():
    opcion = int(input("Desea descargar en formato csv=1, json=2: "))
    generar_periodo()
    descargar_archivo(opcion)


opcion_menu()
